<?php 
    include "header.php";
    include "function.php";
    dd($employees);
?>
<h2 class="text-center m-5">Table</h2>
<table  class="border table table-hover table-striped w-75 m-auto">
    <thead>
        <tr>
            <th>Name</th>
            <th>Gender</th>
            <th>Age</th>
            <th>Position</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($employees as $employee) {?>
            <tr>
                <td><?= $employee->title()?></td>
                <td><?= $employee->getGender()?></td>
                <td><?= $employee->e_age?></td>
                <td><?=  strtoupper($employee->e_position)?></td>
            </tr>
        <?php }?>
    </tbody>
</table>
<?php 
    include "footer.php";
?>