<?php 
    class Employee {
        public $e_name;
        public $e_age;
        public $e_position;
        protected $e_gender;
        function __construct() {

        }
        function setGender($gender = null) {
            return $this->e_gender = $gender;
        }
        function getGender() {
            if($this->e_gender == true) {
                return "Male";
            } elseif($this->e_gender == false) {
                return "Female"; 
            } else {
                return "N/A";
            }
        }
        function title() {
            if($this->e_gender == true) {
                return "Mr. " . $this->e_name;
            } elseif($this->e_gender == false) {
                return "Ms. " . $this->e_name; 
            } else {
                return $this->e_name;
            }
        }
    }
        // dd($people);
?>