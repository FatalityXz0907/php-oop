<?php 
    try {
        $conn = new PDO("mysql:host=localhost; dbname=human_resource", "root","");
    } catch(Exception $e) {
        echo "Can't connect to database";
        die();
    }
    $query = $conn->prepare("select * from employees");
    $query->execute();
    $employees = $query->fetchAll(PDO::FETCH_CLASS, "Employee");
?>